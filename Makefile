all: main.pdf

main.pdf: img

img:
	$(MAKE) -C img

%.pdf: %.tex
	latexmk -pdf $<


clean: clean-img
	latexmk -c

clean-all: clean-img
	latexmk -C

clean-img:
	$(MAKE) -C img clean


.PHONY: all img clean clean-all clean-img
